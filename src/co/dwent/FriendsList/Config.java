package co.dwent.FriendsList;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;


public class Config {

	
	private static FileConfiguration config;
	private static File configFile;
	
	public Config(String FLconfig) {
		configFile = new File(FLconfig);
		load();
	}	
	
	
	
	/*
	 * Configuration Control Methods
	 */
	
	/**
	 * Loads The Config From Disk
	 */
	public static void load()
	{
		config = YamlConfiguration.loadConfiguration(configFile);
	}
	
	/**
	 * Sets A Value To Specified YAML Node
	 * @param node Node To Set
	 * @param value Value To Set For Node
	 */
	public static void setNode(String node, Object value)
	{
		config.set(node, value);
		save();
	}
	
	/**
	 * Saves Current Settings To Config File
	 */
	public static void save() {
		try {
		      config.save(configFile);
		    } catch (IOException ex) {
		      FriendsList.logger.severe("Could not save config to " + configFile);
		      FriendsList.logger.severe(ex.toString());
		    }
	}
	
	/**
	 * Retrieves The Maximum Number Of Friends A Player May Have
	 * @return Returns An Integer Of Maximum Possible Friends
	 */
	public static int getMaxFriends() {
		return config.getInt("MaxUsers", 100);
	}
	
	/**
	 * Checks Whether Friendly PVP Is Enabled
	 * @return Returns True if Enabled
	 */
	public static boolean getPvpEnabled() {
		return config.getBoolean("PVPControl.enable", false);
	}
	
}
