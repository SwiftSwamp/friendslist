package co.dwent.FriendsList;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;


public class Database {

	
	private static FileConfiguration config;
	private static File configFile;
	
	public Database(String dbconfig) {
		configFile = new File(dbconfig);
		load();
	}	
	
	
	
	/*
	 * Database Control Methods
	 */
	
	/**
	 * Loads The Config From Disk
	 */
	public static void load()
	{
		config = YamlConfiguration.loadConfiguration(configFile);
	}
	
	
	/**
	 * Sets A Value To Specified YAML Node
	 * @param node Node To Set
	 * @param value Value To Set For Node
	 */
	public static void setNode(String node, Object value)
	{
		config.set(node, value);
		save();
	}
	
	/**
	 * Saves Current Settings To Config File
	 */
	public static void save() {
		try {
		      config.save(configFile);
		    } catch (IOException ex) {
		      FriendsList.logger.severe("Could not save config to " + configFile);
		      FriendsList.logger.severe(ex.toString());
		    }
	}
	
	/**
	 * Gets A List of Friends A Player Has
	 * @param player Player To Check
	 * @return List Of Friends
	 */
	public static List<String> getFriends(String player) {
		List<String> friends = config.getStringList("friendslistdb." + player);
		return friends;
	}
	
	/**
	 * Adds A Friend To A Players Friend List
	 * @param player Player To Modify List Of
	 * @param targetPlayer Player To Add
	 */
	public static void addFriend(String player, String targetPlayer) {
		List<String> friends = config.getStringList("friendslistdb." + player);
		friends.add(targetPlayer);
		setNode("friendslistdb." + player, friends);
	}
	
	/**
	 * Removes A Friend From A Players Friend List
	 * @param player Player To Modify
	 * @param targetPlayer Player To Remove
	 */
	public static void removeFriend(String player, String targetPlayer) {
		List<String> friends = config.getStringList("friendslistdb." + player);
		friends.remove(targetPlayer);
		setNode("friendslistdb." + player, friends);
	}
	
	/**
	 * Checks If A Player Has Any Friends
	 * @param player Player To Check
	 * @return True If Player Has At Least One Friend
	 */
	public static boolean hasFriends(String player) {
		return  config.getStringList("friendslistdb." + player).size() > 0 ? true : false;
	}
	
	/**
	 * Returns Total Number Of Friends A Player Has
	 * @param player Player To Check
	 * @return Returns Number Of Friends
	 */
	public static int getNumberFriends(String player) {
		return config.getStringList("friendslistdb." + player).size();
	}
}