package co.dwent.FriendsList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class FriendsList extends JavaPlugin{
	public final static Logger logger = Logger.getLogger("Minecraft");
	public static FriendsList plugin;
	
	// Version Info
	public final static String name = "FriendsList";
	public final static String version = "v1.0";
	
	
	//Path Strings And Config Files
	public static String baseDir = "plugins" + File.separator;
	public static String mainDir = baseDir + name + File.separator;
	public static String dbconfig = mainDir + "database.yml";
	public static String FLconfig = mainDir + "config.yml";
	public static Config config = new Config(FLconfig);
	public static Database db = new Database(dbconfig);
	
	// Config Variables
	public static int maxFriends;
	public static boolean friendlyPVP;
	
	
	public void onEnable() {
		baseSetup();
		getServer().getPluginManager().registerEvents((Listener) this, this);
		logger.info("[" + name + " " + version + "] has been enabled!");
	}

	public void onDisable() {
		logger.info("[FriendsList] has been disabled!");
	}
	
	/**
	 * Sets Up Base Directories And Extracts Config Files Before Loading
	 */
	
	private void baseSetup() {
		if ( !(new File(mainDir).exists() ) ) {
			new File(mainDir).mkdir();
		}
		
		if ( !(new File(FLconfig).exists() ) ) {
			extractConfigFile("config.yml",FLconfig);
		}
		
		if ( !(new File(dbconfig).exists() ) ) {
			extractConfigFile("database.yml",dbconfig);
		}
		
		Database.load();
		Config.load();
		
		maxFriends = Config.getMaxFriends();
		friendlyPVP = Config.getPvpEnabled();
	}
	
	
	/**
	 * Extracts Specified Config File From The "default" Folder Inside The Jar
	 * @param name Filename To Extract
	 * @param config File Path To Extract To
	 */
	public void extractConfigFile(String name,String config) {
		File actual = new File(config);

		if (!actual.exists())
		{
			InputStream input = getClass().getResourceAsStream("/default/" + name);
			if (input != null)
			{
				FileOutputStream output = null;
				try
				{
					output = new FileOutputStream(actual);
					byte[] buf = new byte[8192];
					int length = 0;

					while ((length = input.read(buf)) > 0)
					{
						output.write(buf, 0, length);
					}

					logger.info( "Config file extracted and written to disk : " + name);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				finally
				{
					try
					{
						if (input != null)
						{
							input.close();
						}
					}
					catch (Exception e)
					{
					}
					try
					{
						if (output != null)
						{
							output.close();
						}
					}
					catch (Exception e)
					{
					}
				}
			}
		}
	}

	
}